var request = require('request');
var exports = module.exports;
var channel = 'C09AUDQ4B'; // #nyc-office

exports.whatsForLunch = function(user, message, db, expressRes, token) {

  var mongo = db;
  var userName = user;
  var command = message.shift();
  var newLunch = message.join(' ');

  // Let admin announce lunch to a channel.

  if ((userName === 'stephanie.tran' || userName === 'mike.burnett') && command === 'announce') {
    mongo.collection('meals').findOne({'lunch': {'$exists': true}}, function(err, result) {
      if (err) {
        return err;
      }
      var currentLunch = result.lunch;
      var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=nyc-office' + '&text=' + currentLunch + '&as_user=true';
      request.post(uri).on('response', function(response) {
        if (response.statusCode === 200) {
          return expressRes.status(200).end();
        }
      });
    });
  }

  // Let admin update lunch.

  if ((userName === 'stephanie.tran' || userName === 'mike.burnett') && command === 'update') {
    var when = new Date();
    mongo.collection('meals').update(
      {'lunch': {$exists: true}},
      {'lunch': newLunch},
      {upsert: true}, function() {
        var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=Updated lunch.' + '&as_user=true';
        request.post(uri).on('response', function(response) {
          if (response.statusCode === 200) {
            return expressRes.status(200).end();
          }
        });
      });
    } else if (command !== 'announce') {

  // If admin isn't trying to update the lunch, just return what's for lunch.

    var when = new Date();
    mongo.collection('meals').findOne({'lunch': {'$exists': true}}, function(err, result) {
      var currentLunch = '';
      if (result) {
        currentLunch = result.lunch;
      } else {
        currentLunch = 'No lunch has been set.';
      }
      var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=' + currentLunch + '&as_user=true';
      request.post(uri).on('response', function(response) {
        if (response.statusCode === 200) {
          return expressRes.status(200).end();
        }
      });
    });
  }
};
