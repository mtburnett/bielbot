var request = require('request');
var exports = module.exports;

exports.requestDoorcode = function(user, message, db, expressRes, token) {

  var mongo = db;
  var userName = user;
  var command = message[0];
  var newCode = message[1];

  // Let admin update doorcode.

  if ((userName === 'stephanie.tran' || userName === 'mike.burnett') && command == 'update') {
    var when = new Date();
    mongo.collection('passcodes').update(
      {'door': {$exists: true}},
      {'door': newCode},
      {upsert: true}, function() {
        var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=Updated door code.' + '&as_user=true';
        request.post(uri).on('response', function(response) {
          if (response.statusCode === 200) {
            return expressRes.status(200).end();
          }
        });
    });

  } else {

  // If admin isn't trying to update the door code, just return the code.

    var when = new Date();
    mongo.collection('passcodes').findOne({'door': {'$exists': true}}, function(err, result) {
      var doorcodeFromDb = '';
      if (result) {
        doorcodeFromDb = result.door;
      } else {
        doorcodeFromDb = 'No door code has been entered.';
      }
      var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=Door code: ' + doorcodeFromDb + '&as_user=true';
      request.post(uri).on('response', function(response) {
        if (response.statusCode === 200) {
          return expressRes.status(200).end();
        }
      });
    });
  }
};
