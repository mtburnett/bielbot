var request = require('request');
var exports = module.exports;

exports.requestWifiPassword = function(user, message, db, expressRes, token) {

  var mongo = db;
  var userName = user;
  var command = message[0];
  var newPassword = message[1];

  // Let admin update wifi password.

  if ((userName === 'stephanie.tran' || userName === 'mike.burnett') && command == 'update') {
    var when = new Date();
    mongo.collection('passcodes').update(
      {'wifi': {$exists: true}},
      {'wifi': newPassword},
      {upsert: true}, function() {
        var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=Updated wifi password.' + '&as_user=true';
        request.post(uri).on('response', function(response) {
          if (response.statusCode === 200) {
            return expressRes.status(200).end();
          }
        });
      });
    } else {

  // If admin isn't trying to update the wifi password, just return the password.

      var when = new Date();
      mongo.collection('passcodes').findOne({'wifi': {'$exists': true}}, function(err, result) {
        var wifiFromDb = '';
        if (result) {
          wifiFromDb = result.wifi;
        } else {
          wifiFromDb = 'No wifi has been set';
        }
        var uri = 'https://slack.com/api/chat.postMessage' + '?token=' + token + '&channel=@' + userName + '&text=Wifi password: ' + wifiFromDb + '&as_user=true';
        request.post(uri).on('response', function(response) {
          if (response.statusCode === 200) {
            return expressRes.status(200).end();
          }
        });
      });
    }
};
