// bielbot - a slackbot for Blippar developed by Mike Burnett

// Express Setup

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
var port = process.env.BIELBOT_PORT || 3002;
var token = process.env.BIELBOT_TOKEN;
app.locals.token = token;
// Bielbot module contains all command modules for bielbot.

var bielbot = require('./bielbot');

// Routes

app.get('/', function(req, res) {
  res.send('Hello there. I\'m bielbot, and you\'re touching my backend.');
});

app.post('/bielbot', bielbot);

// Error Handler

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(400).send(err.message);
});

// Server Listening

app.listen(port, function() {
  // You can change this port and/or use an environment variable.
  console.log('BielBot listening on port ' + port);
});
