var request = require('request');
var exports = module.exports;

exports.getDevice = function(user, message, db, expressRes) {
  var mongo = db;
  var deviceId = '';
  var deviceModel = '';
  var deviceColor = '';
  var userName = user;
  var deviceUser = '';
  var deviceStatus = '';
  var command = '';
  var checkoutDate;
  var dueDate;
  var returnDate;
  if (userName === 'tanner') {
    var when = new Date();
    var botPayload = {
      channel: '@tanner',
      text: 'You have permission to update devices.',
      username: 'bielbot',
      icon_emoji: ':bielbot:'
    };
    send(botPayload, function(error, status, body) {
      if (error) {
        return next(error);
      } else if (status !== 200) {
        return next(new Error('Incoming WebHook: ' + status + ' ' + body));
      } else {
        return expressRes.status(200).end();
      }
    });
  } else {
    // tell person they don't have permission
  }
  function send(payload, callback) {
    var uri = 'https://hooks.slack.com/services/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';
    request({
      uri: uri,
      method: 'POST',
      body: JSON.stringify(payload)
    }, function (error, response, body) {
      if (error) {
        return callback(error);
      }
      callback(null, response.statusCode, body);
    });
  }
};
