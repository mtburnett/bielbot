// bielbot.js

// Mongo Setup

var mongo = require('mongodb').MongoClient;
var assert = require('assert');
var mongoUrl = 'mongodb://localhost:27017/bielbot';
var db;
mongo.connect(mongoUrl, function(err, database) {
  assert.equal(null, err);
  console.log('Connected correctly to Mongo server.');
  db = database;
});

// Modules

var doorcode = require('./doorcode');
var lunch = require('./lunch');
var device = require('./device');
var wifi = require('./wifi');

// Parse user input and send to proper module.

module.exports = function(req, res) {
  var user = req.body.user_name;
  var input = req.body.text;
  var message = input.split(' ');
  message.replace('&', 'and');
  var command = message.shift(0);
  var token = req.app.locals.token;
  console.log(command, message);
  switch(command) {
    case 'wifi':
      wifi.requestWifiPassword(user, message, db, res, token);
      break;
    case 'doorcode':
      doorcode.requestDoorcode(user, message, db, res, token);
      break;
    case 'lunch':
      lunch.whatsForLunch(user, message, db, res, token);
      break;
    case 'device':
      device.getDevice(user, message, db, res);
      break;
    default:
      console.log('Command not supported.');
  }
};
